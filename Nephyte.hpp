#pragma once

#include <omp.h>
#include <plog/Log.h>
#include <fstream>
#include <string>
#include <vector>
#include <chrono>
#include "FastaFile.hpp"
#include "Hamiltonian.hpp"
#include "Tree.hpp"
#include "Node.hpp"
#include "Sequence.hpp"
#include "Params.hpp"

#ifndef TIMEOUT_MINS
#define TIMEOUT_MINS 5
#endif

using namespace arma;

class Nephyte
{
  public:
    Nephyte(Params parm);
    ~Nephyte();
    void run();
  private:
    Params _parm;
    std::vector<Tree> _trees;
    Sequence* _ancestor;
    Hamiltonian* _hamiltonian;
    HamiltonianEstimator* _hamiltonian_man;
    std::chrono::steady_clock::time_point t0;
    std::chrono::steady_clock::time_point t1;
    void _plantTrees(bool keepUpperBranches);
};
