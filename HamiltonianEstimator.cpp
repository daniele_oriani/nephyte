#include "Hamiltonian.hpp"

HamiltonianEstimator::HamiltonianEstimator(std::vector<Sequence> expmsa)
  : _exp_msa(expmsa)
{
  _current_step = 0;
  _no_survivors = false;
  _hasFrequencies = false;
  _init();
}

void HamiltonianEstimator::setCurrentStep(unsigned step)
{
  _current_step = step;
}

unsigned HamiltonianEstimator::getNumberOfSurvivorLeaves()
{
  return _leaves_info.size();
}

void HamiltonianEstimator::updateHamiltonian(Hamiltonian* H)
{
  //std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
  if(! _hasFrequencies)
  {
    PLOG_VERBOSE << "Hamiltonian update requested but no frequencies were found. Calculating frequencies.";
    _calculateFrequencies();
  }
  if(_no_survivors)
  {
    PLOG_WARNING << "There are no survivor leaves. Skipping hamiltonian update.";
    return;
  }
  double f1_coef = arma::accu((_f1b - _f1b_exp) % _f1b);
  double f2_coef = arma::accu((_f2b - _f2b_exp) % _f2b);
  unsigned s = 0;
  unsigned nleaves = _leaves_info.size();
  unsigned nres = _leaves_info.front().seq->getLength();
  unsigned basetype = _leaves_info.front().seq->getBasetype();
  unsigned chunksize = 10;
  int n_threads = 1;
  sp_mat f1_thetaprime_factor = sp_mat(size(_leaves_info.front().f1b));
  std::vector<sp_mat> f1_thetaprime_factor_tcontr;
  sp_mat f2_thetaprime_factor = sp_mat(_leaves_info.front().f1b.n_rows * _leaves_info.front().f1b.n_rows, _leaves_info.front().f1b.n_cols * _leaves_info.front().f1b.n_cols);
  std::vector<sp_mat> f2_thetaprime_factor_tcontr;
  sp_mat::iterator it;
  sp_mat::iterator it_end;
  sp_mat::iterator it2;
  #pragma omp parallel default(shared) private(s, it, it2, it_end)
  {
    #pragma omp single
    {
      n_threads = omp_get_num_threads();
      for(int thread_n = 0; thread_n < n_threads; ++thread_n)
      {
        f1_thetaprime_factor_tcontr.push_back(sp_mat(size(f1_thetaprime_factor)));
        f2_thetaprime_factor_tcontr.push_back(sp_mat(size(f2_thetaprime_factor)));
      }
    }
    #pragma omp for schedule(static, chunksize)
    for(s = 0; s < nleaves; s++)
    {
      it = _leaves_info.at(s).f1b.begin();
      it_end = _leaves_info.at(s).f1b.end();
      while(it != it_end)
      {
        it2 = _leaves_info.at(s).f1b.begin();
        f1_thetaprime_factor_tcontr.at(omp_get_thread_num())(it.row(), it.col()) += _leaves_info.at(s).thetaprime;
        while(it2 != it_end)
        {
          f2_thetaprime_factor_tcontr.at(omp_get_thread_num())(it.row() * nres + it2.row(), it.col() * basetype + it2.col()) += _leaves_info.at(s).thetaprime;
          ++it2;
        }
        ++it;
      }
    }
    #pragma omp single
    {
      for(int thread_n = 0; thread_n < n_threads; ++thread_n)
      {
        f1_thetaprime_factor += f1_thetaprime_factor_tcontr.at(thread_n);
        f2_thetaprime_factor += f2_thetaprime_factor_tcontr.at(thread_n);
      }
    }
  }
  // Make this factor independent on how many leaves there are.
  // This rescaling is accounted for by the w_0 factor in the update rule
  f1_thetaprime_factor /= (double)nleaves;
  f2_thetaprime_factor /= (double)nleaves;
  double coef = 0.05;//( 1 + currentgen) * mean_mu
  double alpha = 0.03;
  double beta = 1.0;
  double tau = 6000.;
  double f1_thetaprime_nonzero_perc = 100. * double(f1_thetaprime_factor.n_nonzero) / double(f1_thetaprime_factor.n_rows * f1_thetaprime_factor.n_cols);
  double f2_thetaprime_nonzero_perc = 100. * double(f2_thetaprime_factor.n_nonzero) / double(f2_thetaprime_factor.n_rows * f2_thetaprime_factor.n_cols);
  double eta_step = alpha / pow(1 + (double)_current_step / tau, beta);
  double one_b_learning_coef = 0.;//35. * eta_step * coef * f1_coef;
  double two_b_learning_coef = 2. * eta_step * coef * f2_coef; //15. * eta_step * coef * f2_coef;
  PLOG_INFO << "f1_thetaprime_factor nonzero perc = " << f1_thetaprime_nonzero_perc << "%";
  PLOG_INFO << "f2_thetaprime_factor nonzero perc = " << f2_thetaprime_nonzero_perc << "%";
  PLOG_INFO << "Total 1-body learning coefficient = " << one_b_learning_coef;
  PLOG_INFO << "Total 2-body learning coefficient = " << two_b_learning_coef;
  if(f1_thetaprime_nonzero_perc <= 3.0)
  {
    PLOG_WARNING << "One body potential is not being updated! (Not enough nonzero thetaprime factors)";
  }
  if(f2_thetaprime_nonzero_perc <= 3.0)
  {
    PLOG_WARNING << "Two body potential is not being updated! (Not enough nonzero thetaprime factors)";
  }
  std::string f1_message = "Bringing " + std::string((f1_coef < 0) ? "down " : "up ") + std::to_string(f1_thetaprime_nonzero_perc) + "\% of 1 body potential.";
  std::string f2_message = "Bringing " + std::string((f2_coef < 0) ? "down " : "up ") + std::to_string(f2_thetaprime_nonzero_perc) + "\% of 2 body potential.";
  PLOG_INFO << f1_message;
  PLOG_INFO << f2_message;
  #pragma omp parallel sections
  {
    #pragma omp section
    {
      // 1-body potential update
      auto it_1 = f1_thetaprime_factor.begin();
      auto it_1_end = f1_thetaprime_factor.end();
      for(; it_1 != it_1_end; ++it_1)
      {
        unsigned row = it_1.row();
        unsigned col = it_1.col();
        if(col == 0)
        {
          H->_h(row, col) = 0.;
        }
        else
        {
          H->_h(row, col) += one_b_learning_coef * (*it_1);
        }
      }
    }
    #pragma omp section
    {
      // 2-body potential update
      auto it_2 = f2_thetaprime_factor.begin();
      auto it_2_end = f2_thetaprime_factor.end();
      for(; it_2 != it_2_end; ++it_2)
      {
        unsigned row = it_2.row(); // i * nres + j
        unsigned col = it_2.col(); // A * basetype + B
        unsigned i = row / nres;
        unsigned j = row % nres;
        unsigned A = col / basetype;
        unsigned B = col % basetype;
        if(A == 0 || B == 0) // Gap gauge
        {
          H->_J(row, A, B) = 0.;
          H->_J(j * nres + i, B, A) = 0.;
        }
        else
        {
          H->_J(row, A, B) += (two_b_learning_coef * (*it_2)) - 2. * eta_step * 2. * 0.001 * H->_J(row, A, B);
          H->_J(j * nres + i, B, A) = H->_J(row, A, B);
        }
      }
    }
  }
}

std::vector<double> HamiltonianEstimator::getEnergies() const
{
  std::vector<double> res;
  auto it = _leaves_info.begin();
  auto it_end = _leaves_info.end();
  for(; it != it_end; ++it)
    res.push_back(it->energy);
  return res;
}

bool HamiltonianEstimator::deleteLeafInfo(Sequence* s)
{
  bool found = false;
  LeafInfo li0;
  li0.seq = s;
  auto it = std::find_if(_leaves_info.begin(), _leaves_info.end(), [&](const LeafInfo& li1){ return (li1.seq == li0.seq); });
  found = (it != _leaves_info.end());
  if(found)
  {
    // Since order in this particular vector does not matter (it is a vector to
    // benefit from the random access in the omp hamiltonian update), instead
    // of blindly erasing, causing many moves, we just move the found element
    // to the back and then pop it.
    LeafInfo tmp = _leaves_info.back();
    _leaves_info.back() = *it;
    *it = tmp;
    _leaves_info.pop_back();
  }
  return found;
}

void HamiltonianEstimator::_calculateFrequencies()
{
  unsigned nleaves = _leaves_info.size();
  PLOG_INFO << "Total leaves = " << nleaves;
  if(nleaves <= 0)
  {
    _no_survivors = true;
    return;
  }
  unsigned nres = _leaves_info.front().seq->getLength();
  unsigned basetype = _leaves_info.front().seq->getBasetype();
  unsigned s = 0;
  unsigned i = 0;
  unsigned j = 0;
  unsigned ri = 0;
  unsigned chunksize = 10;
  _f1b.zeros(nres, basetype);
  _f2b.zeros(nres * nres, basetype, basetype);
  std::vector<mat> f1b_thread;
  std::vector<cube> f2b_thread;
  unsigned n_threads = 0;
  #pragma omp parallel default(shared) private(s)
  {
    #pragma omp for schedule(static, 1)
    for(s = 0; s < nleaves; s++)
    {
      _leaves_info.at(s).f1b.zeros(nres, basetype);
    }
  }
  s = 0;
  #pragma omp parallel default(shared) private(s, i, j, ri)
  {
    #pragma omp single
    {
      n_threads = omp_get_num_threads();
      for(unsigned t = 0; t < n_threads; ++t)
      {
        f1b_thread.emplace_back(mat(size(_f1b), fill::zeros));
        f2b_thread.emplace_back(cube(size(_f2b), fill::zeros));
      }
    }
    #pragma omp for schedule(static, chunksize)
    for(s = 0; s < nleaves; s++) 
    {
      for(i = 0; i < nres; i++)
      {
        ri = unsigned(_leaves_info.at(s).seq->getResidue(i));
        f1b_thread.at(omp_get_thread_num())(i, ri) += 1.0;
        _leaves_info.at(s).f1b(i, ri) = 1.0;
        for(j = 0; j < nres; j++)
        {
          f2b_thread.at(omp_get_thread_num())(i * nres + j, ri, unsigned(_leaves_info.at(s).seq->getResidue(j))) += 1.0;
        }
      }
    }
  }
  for(unsigned tn = 0; tn < n_threads; ++tn)
  {
    _f1b += f1b_thread.at(tn);
    _f2b += f2b_thread.at(tn);
  }
  _f1b /= (double)nleaves;
  _f2b /= (double)nleaves;
  _hasFrequencies = true;
}

arma::cube* HamiltonianEstimator::getF2bExp()
{
  return &_f2b_exp;
}

arma::mat* HamiltonianEstimator::getF1bExp()
{
  return &_f1b_exp;
}

arma::cube* HamiltonianEstimator::getF2b()
{
  if(! _hasFrequencies)
  {
    PLOG_VERBOSE << "2 body frequency was requested, but it has not been calculated yet. Calculating it now.";
    _calculateFrequencies();
  }
  return &_f2b;
}

arma::mat* HamiltonianEstimator::getF1b()
{
  if(! _hasFrequencies)
  {
    PLOG_VERBOSE << "1 body frequency was requested, but it has not been calculated yet. Calculating it now.";
    _calculateFrequencies();
  }
  return &_f1b;
}

double HamiltonianEstimator::getChi2F1b()
{
  double res = 0.;
  if(! _hasFrequencies)
  {
    PLOG_VERBOSE << "Chi2 calculation was requested by getChi2 but no frequencies were found. Calculating frequencies.";
    _calculateFrequencies();
  }
  res = accu((_f1b - _f1b_exp) % (_f1b - _f1b_exp));
  res /= (double)_f1b.n_elem; // N(N-1)/2 ~ N*N/2
  return res;
}

double HamiltonianEstimator::getChi2F2b(const double& regul)
{
  double res = 0.;
  if(! _hasFrequencies)
  {
    PLOG_VERBOSE << "Chi2 calculation was requested by getChi2 but no frequencies were found. Calculating frequencies.";
    _calculateFrequencies();
  }
  res = accu((_f2b - _f2b_exp) % (_f2b - _f2b_exp));
  res /= (double)_f2b.n_elem / 2.; // N(N-1)/2 ~ N*N/2
  res += regul;
  return res;
}

void HamiltonianEstimator::_init()
{
  unsigned nres = _exp_msa.front().getLength();
  unsigned basetype = _exp_msa.front().getBasetype();
  _f1b_exp.zeros(nres, basetype);
  _f2b_exp.zeros(nres * nres, basetype, basetype);
  unsigned nleaves = _exp_msa.size();
  unsigned s = 0;
  unsigned i = 0;
  unsigned j = 0;
  unsigned chunksize = 25;
  #pragma omp parallel default(shared) private(s, i, j)
  {
    #pragma omp for schedule(static, chunksize)
    for(i = 0; i < nres; i++) // inverted for loops to avoid data races in _f1b and _f2b;
    {
      for(s = 0; s < nleaves; s++)
      {
        unsigned ri = unsigned(_exp_msa.at(s).getResidue(i));
        _f1b_exp(i, ri) += 1.0;
        for(j = 0; j < nres; j++)
        {
          _f2b_exp(i * nres + j, ri, unsigned(_exp_msa.at(s).getResidue(j))) += 1.0;
        }
      }
    }
  }
  _f1b_exp /= (double)nleaves;
  _f2b_exp /= (double)nleaves;
}

void HamiltonianEstimator::reset()
{
  _leaves_info.clear();
  _no_survivors = false;
  _f1b.zeros();
  _f2b.zeros();
  _hasFrequencies = false;
}
