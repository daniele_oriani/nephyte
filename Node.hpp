#pragma once

#include "Sequence.hpp"
#include <string>
#include <list>
#include <iostream> // TODO remove

class Node
{
  public:
    Node(std::string nodename, Node* parent, Sequence* seq);
    ~Node();
    Node(const Node& node);
    //Node(Node&& othernode);
    bool isRoot() const;
    void addChild(Node* child);
    bool rmChild(std::string nodename);
    std::string getNodeName() const;
    Node* getParent() const;
    Sequence* getSequence() const;
    std::list<Node*>* getChildrenRef();
    std::list<Node*> getChildren() const;
    unsigned getNumberOfChildren() const;
  private:
    std::string _nodename;
    Node* _parent;
    std::list<Node*> _children;
    Sequence* _seq;
    unsigned _nofchildren;
};
