#pragma once

#include <string>
#include <vector>

typedef unsigned char letter_t;
typedef std::vector<letter_t> word_t;

class Sequence
{
  public:
    Sequence(const unsigned basetype);
    Sequence(const unsigned basetype, const word_t& data);
    Sequence(const unsigned basetype, const std::string& data);
    Sequence(const Sequence& seq);
    ~Sequence();
    word_t getDigits() const;
    std::string getLetters() const;
    unsigned getBasetype() const;
    unsigned getLength() const;
    letter_t getResidue(unsigned pos) const;
    void setResidue(unsigned pos, letter_t res);
    bool operator==(const Sequence& rhs);
    bool operator!=(const Sequence& rhs);
  protected:
    unsigned _basetype;
    word_t _data;
  private:
    word_t _lettersToDigits(const std::string& in) const;
    std::string _digitsToLetters(const word_t& in) const;
};
