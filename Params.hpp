#pragma once

struct Params
{
  bool singleStep;
  bool passThru;
  unsigned nSteps;
  unsigned nTrees;
  unsigned nBranches;
  unsigned basetype;
  unsigned nThreads;
  unsigned printStep;
  unsigned mutationsPerGeneration;
  unsigned maxLeaves;
  double dissimilarity;
  double sigmoidSteepness;
  std::string ancestorFile;
  std::string expmsaFile;
  std::string h0File;
  std::string J0File;
  std::string outDir;
};
