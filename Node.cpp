#include "Node.hpp"

Node::Node(std::string nodename, Node* parent, Sequence* seq)
  : _nodename(nodename), _parent(parent), _seq(seq)
{
  _nofchildren = 0;
  _children = std::list<Node*>();
}

Node::~Node()
{
  _parent = nullptr;
  _seq = nullptr;
  _children.clear();
}

Node::Node(const Node& node)
{
  this->_nodename = node.getNodeName();
  this->_parent = node.getParent();
  this->_seq = node.getSequence();
  this->_nofchildren = node.getNumberOfChildren();
  this->_children = node.getChildren();
}

//Node::Node(Node&& othernode)
//{
  //this->_children = othernode.getChildren();
  //othernode.getChildrenRef()->clear();
  //this->_nodename = othernode.getNodeName();
  //this->_parent = othernode.getParent();
  //this->_seq = othernode.getSequence();
  //this->_nofchildren = othernode.getNumberOfChildren();
//}

bool Node::isRoot() const
{
  return (_parent == 0 || _parent == nullptr);
}

void Node::addChild(Node* child)
{
  _nofchildren++;
  _children.emplace_back(child);
}

bool Node::rmChild(std::string nodename)
{
  bool res = false;
  auto it = _children.begin();
  auto it_end = _children.end();
  while(it != it_end)
  {
    if((*it)->getNodeName() == nodename)
    {
      auto it_tmp = it;
      it++;
      _children.erase(it_tmp);
      _nofchildren--;
      res = true;
      break;
    }
    else
      it++;
  }
  return res;
}

std::string Node::getNodeName() const
{
  return _nodename;
}

Node* Node::getParent() const
{
  return _parent;
}

Sequence* Node::getSequence() const
{
  return _seq;
}

std::list<Node*> Node::getChildren() const
{
  return _children;
}

std::list<Node*>* Node::getChildrenRef()
{
  return &_children;
}

unsigned Node::getNumberOfChildren() const
{
  return _nofchildren;
}
