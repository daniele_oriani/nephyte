#include <iostream>
#include <string>
#include "cxxopts.hpp"
#include "Params.hpp"
#include <plog/Log.h>
#include <plog/Appenders/ConsoleAppender.h>

#ifndef COMPATIBILITY_MODE
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#define BOOL2STR(b) (b ? "true" : "false")

Params parseOpts(int argc, char** argv)
{
  int cli_argc = argc;
  Params res;
  cxxopts::Options options(argv[0], "Nephyte: Non-equilibrium phylogenetic tree engine");
  options.add_options()
  ("s,steps", "Total number of steps, i.e. hamiltonian updates", cxxopts::value<unsigned>()->default_value("1"), "INT")
  ("p,printstep", "Step intervals between different prints of control data", cxxopts::value<unsigned>()->default_value("1"), "INT")
  ("b,branches", "Branching ratio", cxxopts::value<unsigned>()->default_value("4"), "INT")
  ("B,basetype", "Number of different residues (only 20 or 21 are currently supported)", cxxopts::value<unsigned>()->default_value("21"), "INT")
  ("c,threads", "Number of threads to use with openmp (0 means use system default value)", cxxopts::value<unsigned>()->default_value("0"), "INT")
  ("d,dissimilarity", "Dissimilarity threshold wrt ancestor (when to stop tree?)", cxxopts::value<double>()->default_value("0.5"), "DOUBLE")
  ("S,sigmoid-steepness", "Steepness of the sigmoid acceptance predicate", cxxopts::value<double>()->default_value("5.0"), "DOUBLE")
  ("T,trees", "Number of trees to generate in each step", cxxopts::value<unsigned>()->default_value("10"), "INT")
  ("f,family", "Family fasta input file", cxxopts::value<std::string>()->default_value(""), "FILE")
  ("A,ancfile", "Ancestor fasta input file", cxxopts::value<std::string>()->default_value(""), "FILE")
  ("J,jfile", "Use specified armadillo tensor as initial J", cxxopts::value<std::string>()->default_value(""), "FILE")
  ("H,hfile", "Use specified armadillo matrix as initial h", cxxopts::value<std::string>()->default_value(""), "FILE")
  ("w,out-dir", "Save results in specified directory", cxxopts::value<std::string>()->default_value(""), "FILE")
  ("m,mutations-per-generation", "Number of punctual mutations to perform on each sequence in each generation", cxxopts::value<unsigned>()->default_value("5"), "INT")
  ("M,max-leaves", "Number of maximum total leaves (0 means no maximum)", cxxopts::value<unsigned>()->default_value("0"), "INT")
  ("P,pass-thru", "Accept all generated sequences", cxxopts::value<bool>()->default_value("false"), "BOOL")
  ("1,single-step", "Generate trees and print them, leaving the hamiltonian unchanged (overrides -s and -p)", cxxopts::value<bool>()->default_value("false"), "BOOL")
  ("q,quiet", "Quiet output. Use -qq to be even quieter.", cxxopts::value<bool>()->default_value("false"), "BOOL")
  ("v,verbose", "Verbose output (overrides -q). Use -vv to be even more verbose.", cxxopts::value<bool>()->default_value("false"), "BOOL")
  ("h,help", "Print this help message")
  ;
  auto result = options.parse(argc, argv);
  if(cli_argc < 2 || result.count("h"))
  {
    std::cout << options.help({""}) << std::endl;
    exit(0);
  }
  int howQuiet = 0;
  int howVerbose = 0;
  res.nSteps = result["s"].as<unsigned>();
  res.printStep = result["p"].as<unsigned>();
  res.nBranches = result["b"].as<unsigned>();
  res.basetype = result["B"].as<unsigned>();
  res.nThreads = result["c"].as<unsigned>();
  res.nTrees = result["T"].as<unsigned>();
  res.dissimilarity = result["d"].as<double>();
  res.h0File = result["H"].as<std::string>();
  res.J0File = result["J"].as<std::string>();
  res.sigmoidSteepness = result["S"].as<double>();
  res.mutationsPerGeneration = result["m"].as<unsigned>();
  res.maxLeaves = result["M"].as<unsigned>();
  res.expmsaFile = result["f"].as<std::string>();
  res.singleStep = result["1"].as<bool>();
  res.passThru = result["P"].as<bool>();
  bool quiet = result["q"].as<bool>();
  howQuiet = result.count("q");
  bool verbose = result["v"].as<bool>();
  howVerbose = result.count("v");
  res.outDir = result["w"].as<std::string>();
  if(! res.outDir.empty())
  {
    if(res.outDir.back() != '/')
      res.outDir.append("/");
  #ifndef COMPATIBILITY_MODE
    struct stat st = {0};
    if (stat(res.outDir.c_str(), &st) == -1)
    {
      if(mkdir(res.outDir.c_str(), 0700) == -1)
      {
        std::cerr << "An error occurred while creating the output directory. Aborting." << std::endl;
        exit(1);
      }
    }
  }
  #endif
  static plog::ConsoleAppender<plog::TxtFormatter> consoleAppender;
  plog::Severity verbosity = plog::info;
  if(quiet)
  {
    if(howQuiet == 1)
      verbosity = plog::warning;
    else
      verbosity = plog::error;
  }
  if(verbose)
  {
    if(howVerbose == 1)
      verbosity = plog::debug;
    else
      verbosity = plog::verbose;
  }
  plog::init(verbosity, (res.outDir + "nephyte.log").c_str()).addAppender(&consoleAppender);
  PLOG_DEBUG << "Logger initialized";
  res.ancestorFile = result["A"].as<std::string>();
  if(res.ancestorFile.empty())
  {
    PLOG_FATAL << "You must always provide an ancestor.";
    exit(1);
  }
  if(res.singleStep)
  {
    res.nSteps = 1;
    res.printStep = 1;
  }
  if(!res.singleStep && res.expmsaFile.empty())
  {
    PLOG_FATAL << "Error! Unless you want to work with a fixed potential (-1 option), then you must specify an experimental MSA file (-f option).";
    exit(1);
  }
  PLOG_VERBOSE << "CLI Arguments:"            << std::endl
               << "step = "                   << res.nSteps                 << std::endl
               << "prints = "                 << res.printStep              << std::endl
               << "branches = "               << res.nBranches              << std::endl
               << "trees = "                  << res.nTrees                 << std::endl
               << "threads = "                << res.nThreads               << std::endl
               << "dissimilarity = "          << res.dissimilarity          << std::endl
               << "sigmoid steepness = "      << res.sigmoidSteepness       << std::endl
               << "mutations / generation = " << res.mutationsPerGeneration << std::endl
               << "max leaves = "             << res.maxLeaves              << std::endl
               << "Initial J file = "         << res.J0File.c_str()         << std::endl
               << "Initial h file = "         << res.h0File.c_str()         << std::endl
               << "Output directory = "       << res.outDir.c_str()         << std::endl
               << "family_input_file = "      << res.expmsaFile.c_str()     << std::endl
               << "anc input file = "         << res.ancestorFile.c_str()   << std::endl
               << "single step = "            << BOOL2STR(res.singleStep)   << std::endl
               << "pass thru = "              << BOOL2STR(res.passThru)     << std::endl;
  return res;
}
