#include "FastaFile.hpp"

FastaFile::FastaFile(const unsigned basetype, std::string filename)
  : _basetype(basetype), _filename(filename)
{
  _buf = std::ifstream(_filename); 
  std::string s;
  if(!_buf.good())
  {
    PLOG_ERROR << "File " << _filename.c_str() << " is bad!";
  }
  std::getline(_buf, s);
  while(_buf.good())
  {
    if(s.front() != '>')
    {
      Sequence seq(_basetype, s);
      _sequences.push_back(seq);
    }
    std::getline(_buf, s);
  }
  _buf.close();
}

std::vector<Sequence> FastaFile::getMSA()
{
  return _sequences;
}
