#include "Sequence.hpp"

Sequence::Sequence(const unsigned basetype)
  : _basetype(basetype)
{
}

Sequence::Sequence(const unsigned basetype, const word_t& data)
  : _basetype(basetype), _data(data)
{
}

Sequence::Sequence(const unsigned basetype, const std::string& data)
  : _basetype(basetype)
{
  _data = _lettersToDigits(data);
}

Sequence::Sequence(const Sequence& seq)
{
  _basetype = seq.getBasetype();
  _data = seq.getDigits();
}

Sequence::~Sequence()
{
  _data.clear();
}

word_t Sequence::getDigits() const
{
  return _data;
}

std::string Sequence::getLetters() const
{
  return _digitsToLetters(_data);
}

unsigned Sequence::getBasetype() const
{
  return _basetype;
}

unsigned Sequence::getLength() const
{
  return _data.size();
}

letter_t Sequence::getResidue(unsigned pos) const
{
  return _data.at(pos);
}

void Sequence::setResidue(unsigned pos, letter_t res)
{
  _data.at(pos) = res;
}

bool Sequence::operator==(const Sequence& rhs)
{
  return (this->getBasetype() == rhs.getBasetype()) && (this->getDigits() == rhs.getDigits());
}

bool Sequence::operator!=(const Sequence& rhs)
{
  return !(*this == rhs);
}

std::string Sequence::_digitsToLetters(const word_t& in) const
{
  std::string s = "";
  unsigned nelem = in.size();
  for(unsigned k = 0; k < nelem; ++k)
  {
    letter_t AA = in.at(k);
    if(_basetype == 20)
      AA++;
    if(AA == 0)
      s += "-";
    else if(AA == 1)
      s += "A";
    else if(AA == 2)
      s += "C";
    else if(AA == 3)
      s += "D";
    else if(AA == 4)
      s += "E";
    else if(AA == 5)
      s += "F";
    else if(AA == 6)
      s += "G";
    else if(AA == 7)
      s += "H";
    else if(AA == 8)
      s += "I";
    else if(AA == 9)
      s += "K";
    else if(AA == 10)
      s += "L";
    else if(AA == 11)
      s += "M";
    else if(AA == 12)
      s += "N";
    else if(AA == 13)
      s += "P";
    else if(AA == 14)
      s += "Q";
    else if(AA == 15)
      s += "R";
    else if(AA == 16)
      s += "S";
    else if(AA == 17)
      s += "T";
    else if(AA == 18)
      s += "V";
    else if(AA == 19)
      s += "W";
    else if(AA == 20)
      s += "Y";
  }
  return s;
}

word_t Sequence::_lettersToDigits(const std::string& in) const
{
  unsigned nelem = in.length();
  word_t res(nelem);
  for(unsigned k = 0; k < nelem; ++k)
  {
    char s = in.at(k);
    if(s == '-')
      res.at(k) = 0;
    else if(s == 'A')
      res.at(k) = 1;
    else if(s == 'C')
      res.at(k) = 2;
    else if(s == 'D')
      res.at(k) = 3;
    else if(s == 'E')
      res.at(k) = 4;
    else if(s == 'F')
      res.at(k) = 5;
    else if(s == 'G')
      res.at(k) = 6;
    else if(s == 'H')
      res.at(k) = 7;
    else if(s == 'I')
      res.at(k) = 8;
    else if(s == 'K')
      res.at(k) = 9;
    else if(s == 'L')
      res.at(k) = 10;
    else if(s == 'M')
      res.at(k) = 11;
    else if(s == 'N')
      res.at(k) = 12;
    else if(s == 'P')
      res.at(k) = 13;
    else if(s == 'Q')
      res.at(k) = 14;
    else if(s == 'R')
      res.at(k) = 15;
    else if(s == 'S')
      res.at(k) = 16;
    else if(s == 'T')
      res.at(k) = 17;
    else if(s == 'V')
      res.at(k) = 18;
    else if(s == 'W')
      res.at(k) = 19;
    else if(s == 'Y')
      res.at(k) = 20;
    if(_basetype == 20)
      res.at(k)--;
  }
  return res;
}
