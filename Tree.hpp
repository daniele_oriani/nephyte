#pragma once

#include "Node.hpp"
#include "Hamiltonian.hpp"
#include <list>
#include <random>
#include <chrono>
#include <plog/Log.h>

class Tree
{
  public:
    Tree(Node root, bool keepUpperBranches = false);
    ~Tree();
    void clear();
    void prune(const double& prob, const bool& isFinalGeneration, const Hamiltonian& H);
    unsigned getNodesCount() const;
    unsigned getLeavesCount() const;
    unsigned getCurrentGeneration() const;
    Node* getRoot() const;
    std::string getNewick(Node* r = nullptr) const;
    std::string getNexus(std::string treename) const;
    std::string getLeavesMSA() const;
    bool isDead() const;
    bool isKeepingUpperBranches() const;
    void spawnBranches(unsigned branches);
    void mutateLeaves();
    void naturalSelection(const Hamiltonian& H, bool isFinalGeneration);
  private:
    bool _keepUpperBranches;
    bool _isDead;
    unsigned _currentGeneration;
    unsigned _basetype;
    unsigned _nres;
    mutable Node* _root;
    std::list<Node> _upper_nodes;
    std::list<Node> _leaf_nodes;
    std::list<Sequence> _leaves_msa;
    std::list<Sequence> _upper_branches_msa;
    std::default_random_engine _rnd_generator;
    std::uniform_int_distribution<unsigned> _residue_position_distribution;
    std::uniform_int_distribution<letter_t> _residue_distribution;
    std::uniform_real_distribution<double> _01_distribution;
};
