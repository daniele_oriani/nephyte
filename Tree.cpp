#include "Tree.hpp"

Tree::Tree(Node root, bool keepUpperBranches)
  : _keepUpperBranches(keepUpperBranches)
{
  _isDead = false;
  _currentGeneration = 0;
  _leaf_nodes.push_front(root);
  _leaves_msa.push_front(*(root.getSequence()));
  _root = &(*(_leaf_nodes.begin()));
  _basetype = _root->getSequence()->getBasetype();
  _nres = _root->getSequence()->getLength();
  // Seed rand generator
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  _rnd_generator = std::default_random_engine(seed);
  // Initialize rand distributions
  _residue_position_distribution = std::uniform_int_distribution<unsigned>(0, _nres - 1);
  _residue_distribution = std::uniform_int_distribution<letter_t>(0, _basetype - 1);
  _01_distribution = std::uniform_real_distribution<double>(0.0,1.0);
}

Tree::~Tree()
{
  _root = nullptr;
  _upper_nodes.clear();
  _leaf_nodes.clear();
  _leaves_msa.clear();
  _upper_branches_msa.clear();
}

void Tree::clear()
{
  _upper_nodes.clear();
  _leaf_nodes.clear();
  _leaves_msa.clear();
  _upper_branches_msa.clear();
}

void Tree::prune(const double& prob, const bool& isFinalGeneration, const Hamiltonian& H)
{
  if(_isDead)
  {
    return;
  }
  if(_keepUpperBranches)
  {
    auto it = _leaf_nodes.begin();
    auto it_end = _leaf_nodes.end();
    auto it_msa = _leaves_msa.begin();
    auto it_msa_end = _leaves_msa.end();
    while(it != it_end && it_msa != it_msa_end)
    {
      // Last generation cannot be pruned cleanly yet
      bool trim = (_01_distribution(_rnd_generator) <= prob) & !isFinalGeneration;
      if(trim)
      {
        bool removal = it->getParent()->rmChild(it->getNodeName());
        if(!removal)
        {
          PLOG_ERROR << "Error while removing child " << it->getNodeName() << " from its parent children list.";
        }
        auto it_tmp = it;
        auto it_msa_tmp = it_msa;
        it++;
        it_msa++;
        if(*(it_tmp->getSequence()) == *it_msa_tmp)
        {
          _leaf_nodes.erase(it_tmp);
          _leaves_msa.erase(it_msa_tmp);
        }
        else
        {
          PLOG_WARNING << "Remove instead of erase";
          _leaves_msa.remove(*(it_tmp->getSequence()));
          _leaf_nodes.erase(it_tmp);
        }
      }
      else
      {
        it++;
        it_msa++;
      }
    }
  }
  else
  {
    auto it_msa = _leaves_msa.begin();
    auto it_msa_end = _leaves_msa.end();
    while(it_msa != it_msa_end)
    {
      // Last generation cannot be pruned cleanly yet
      bool trim = (_01_distribution(_rnd_generator) <= prob) & !isFinalGeneration;
      if(trim)
      {
        std::list<Sequence>::iterator it_msa_tmp = it_msa;
        it_msa++;
        _leaves_msa.erase(it_msa_tmp);
      }
      else
        it_msa++;
    }
    if(_leaves_msa.empty() || _leaf_nodes.empty())
    {
      PLOG_VERBOSE << "Tree in thread "  << omp_get_thread_num() << " died.";
      _isDead = true;
      return;
    }
  }
}

bool Tree::isKeepingUpperBranches() const
{
  return _keepUpperBranches;
}

unsigned Tree::getCurrentGeneration() const
{
  return _currentGeneration;
}

unsigned Tree::getNodesCount() const
{
  return _upper_branches_msa.size();
}

unsigned Tree::getLeavesCount() const
{
  return _leaves_msa.size();
}

Node* Tree::getRoot() const
{
  return _root;
}

std::string Tree::getNewick(Node* r) const
{
  if(! _keepUpperBranches)
  {
    PLOG_ERROR << "Tree was not keeping upper branches, yet a complete tree was asked!";
    return std::string("");
  }
  std::string res = "";
  std::string suffix = "";
  if(r == 0 || r == nullptr)
  {
    r = _root;
    suffix = ";";
  }
  bool a = (r == _root);
  printf("isDead = %c ; ", _isDead ? 'T' : 'F');
  printf("Processing root = %c ; ", a ? 'T' : 'F');
  unsigned nchildren = r->getChildrenRef()->size();
  printf("nofchildren = %d\n", nchildren);
  if(nchildren > 0)
  {
    res += "(";
    auto it = r->getChildrenRef()->begin();
    auto it_end = r->getChildrenRef()->end();
    for(; it != it_end; ++it)
    {
      try
      {
        auto it_next = it++;
        if(it_next != it_end)
          res += this->getNewick(*it) + ", ";
        else
          res += this->getNewick(*it);
      }
      catch(std::exception& e)
      {
        PLOG_ERROR << "Exception caught: " << e.what();
        return r->getNodeName();
      }
    }
  }
  else
  {
    return r->getNodeName();
  }
  res += ")" + r->getNodeName() + suffix;
  return res;
}

std::string Tree::getNexus(std::string treename) const
{
  if(_leaves_msa.empty() || _leaf_nodes.empty())
  {
    PLOG_ERROR << "No tree for thread "  << omp_get_thread_num() << ": tree dead";
    return std::string();
  }
  auto it = _upper_nodes.begin();
  auto end = _upper_nodes.end();
  std::string ntax = std::to_string(this->getNodesCount());
  std::string taxa = "";
  std::string datamatrix = "";
  std::string trees = "";//"Tree " + treename + "=" + this->getNewick();
  std::string nchar = std::to_string(it->getSequence()->getLength());
  for(; it != end ; ++it)
  {
    taxa.append(" " + it->getNodeName());
    std::string sep = "";
    while(sep.length() + it->getNodeName().length() < 10)
      sep += " ";
    datamatrix += "    " + it->getNodeName() + sep + it->getSequence()->getLetters() + "\n";
  }
  it = _leaf_nodes.begin();
  end = _leaf_nodes.end();
  for(; it != end ; ++it)
  {
    taxa.append(" " + it->getNodeName());
    std::string sep = "";
    while(sep.length() + it->getNodeName().length() < 10)
      sep += " ";
    datamatrix += "    " + it->getNodeName() + sep + it->getSequence()->getLetters() + "\n";
  }
  std::string nexus = R"RAW(#NEXUS
Begin taxa;
  Dimensions NTax=)RAW";
  nexus.append(ntax);
  nexus.append(";\n  TaxLabels" + taxa + ";");
  nexus.append(R"RAW(
 End;

Begin data;
  Dimensions NChar=)RAW" + nchar + ";");
  nexus.append(R"RAW(
  Format DataType=Protein;
  Matrix
)RAW" + datamatrix + ";\nEnd;\n");

  nexus.append("\nBegin trees;\n" + trees + "\nEnd;");

  return nexus;

}

std::string Tree::getLeavesMSA() const
{
  auto it = _leaves_msa.begin();
  auto it_end = _leaves_msa.end();
  std::string res;
  for(; it != it_end; ++it)
    res += it->getLetters() + "\n";
  return res;
}

bool Tree::isDead() const
{
  return _isDead;
}

void Tree::spawnBranches(unsigned branches)
{
  if(_isDead)
    return;
  _currentGeneration++;
  if(_keepUpperBranches)
  {
    unsigned seqIndex = 0;
    bool lastIteration;
    auto it = _leaf_nodes.begin();
    Node* lastNode = &(_leaf_nodes.back());
    _upper_branches_msa.splice(_upper_branches_msa.end(), _leaves_msa);
    _upper_nodes.splice(_upper_nodes.end(), _leaf_nodes);
    if(branches == 0)
      return;
    while(true)
    {
      lastIteration = (&(*it) == lastNode);
      for(unsigned b = 0; b < branches; ++b)
      {
        Sequence s = *(it->getSequence());
        _leaves_msa.push_front(s);
        Node n(std::string("G") + std::to_string(_currentGeneration) + std::string("_S") + std::to_string(seqIndex), &(*it), &(*(_leaves_msa.begin())));
        _leaf_nodes.emplace_front(n);
        it->addChild(&(*(_leaf_nodes.begin())));
        seqIndex++;
      }
      if(lastIteration)
        break;
      else
        ++it;
    }
  }
  else
  {
    std::list<Sequence> tmp;
    auto it = _leaves_msa.begin();
    auto it_end = _leaves_msa.end();
    for(; it != it_end; ++it)
    {
      for(unsigned b = 0; b < branches; ++b)
      {
        Sequence s = *it;
        tmp.push_back(s);
      }
    }
    _leaves_msa.clear();
    _leaves_msa.splice(_leaves_msa.begin(), tmp);
  }
}

void Tree::mutateLeaves()
{
  if(_isDead)
    return;
  auto it = _leaves_msa.begin();
  auto it_end = _leaves_msa.end();
  for(; it != it_end; ++it)
  {
    unsigned rnd_pos = _residue_position_distribution(_rnd_generator);   
    letter_t rnd_res = _residue_distribution(_rnd_generator);   
    letter_t cur_res = it->getResidue(rnd_pos);
    while(rnd_res == cur_res)
      rnd_res = _residue_distribution(_rnd_generator);   
    it->setResidue(rnd_pos, rnd_res);
  }
}

void Tree::naturalSelection(const Hamiltonian& H, bool isFinalGeneration)
{
  if(_isDead)
  {
    return;
  }
  if(_keepUpperBranches)
  {
    auto it = _leaf_nodes.begin();
    auto it_end = _leaf_nodes.end();
    auto it_msa = _leaves_msa.begin();
    auto it_msa_end = _leaves_msa.end();
    while(it != it_end && it_msa != it_msa_end)
    {
      bool accept = H.validate(it->getSequence(), isFinalGeneration);
      if(!accept)
      {
        bool removal = it->getParent()->rmChild(it->getNodeName());
        if(!removal)
        {
          PLOG_ERROR << "Error while removing child " << it->getNodeName() << " from its parent children list.";
        }
        auto it_tmp = it;
        auto it_msa_tmp = it_msa;
        it++;
        it_msa++;
        if(*(it_tmp->getSequence()) == *it_msa_tmp)
        {
          _leaf_nodes.erase(it_tmp);
          _leaves_msa.erase(it_msa_tmp);
        }
        else
        {
          PLOG_WARNING << "Remove instead of erase";
          _leaves_msa.remove(*(it_tmp->getSequence()));
          _leaf_nodes.erase(it_tmp);
        }
      }
      else
      {
        it++;
        it_msa++;
      }
    }
  }
  else
  {
    auto it_msa = _leaves_msa.begin();
    auto it_msa_end = _leaves_msa.end();
    while(it_msa != it_msa_end)
    {
      bool accept = H.validate(&(*it_msa), isFinalGeneration);
      if(!accept)
      {
        std::list<Sequence>::iterator it_msa_tmp = it_msa;
        it_msa++;
        _leaves_msa.erase(it_msa_tmp);
      }
      else
        it_msa++;
    }
  }
  if(_leaves_msa.empty() || _leaf_nodes.empty())
  {
    PLOG_VERBOSE << "Tree in thread "  << omp_get_thread_num() << " died.";
    _isDead = true;
    return;
  }
}
