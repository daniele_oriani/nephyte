#include "Hamiltonian.hpp"

Hamiltonian::Hamiltonian(mat h, cube J, HamiltonianEstimator* estimator, Params* parm)
  : _parm(parm), _h(h), _J(J), _myEstimator(estimator)
{
  _sigmoidSteepness = _parm->sigmoidSteepness;
  _energy_gt_thresh = 0;
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  _rnd_generator = std::default_random_engine(seed);
  _01_distribution = std::uniform_real_distribution<double>(0.0, 1.0);
}

void Hamiltonian::setSigmoidSteepness(double x)
{
  _sigmoidSteepness = x;
}

void Hamiltonian::setEnergyThreshold(double x)
{
  _energyThreshold = x;
}

double Hamiltonian::getJsquared() const
{
  return accu(_J % _J);
}

bool Hamiltonian::validate(Sequence* s, bool isFinalGeneration) const
{
  double seq_energy = getSequenceEnergy(s);
  double seq_thetaprime = 0.;
  bool res = _thresholdPredicate(seq_energy, seq_thetaprime);
  if(res && isFinalGeneration && (! _parm->singleStep))
  {
    LeafInfo li;
    li.seq = s;
    li.thetaprime = seq_thetaprime;
    li.energy = seq_energy;
    #pragma omp critical
    {
      _myEstimator->_leaves_info.emplace_back(li);
    }
  }
  return res;
}

double Hamiltonian::getSequenceEnergy(Sequence* s) const
{
  double res = 0;
  unsigned len = s->getLength();
  #ifdef CONSISTENCY_CHECK
  unsigned basetype = s->getBasetype();
  if(len != _h.n_rows || basetype != _h.n_cols)
  {
    PLOG_ERROR << "Error: sequence and 1 body potential use different residue numbers or basetypes!";
  }
  if(len * len != _J.n_rows || basetype != _J.n_cols || basetype != _J.n_slices)
  {
    PLOG_ERROR << "Error: sequence and 2 body potential use different residue numbers or basetypes!";
  }
  #endif
  for(unsigned i = 0; i < len; ++i)
  {
    unsigned ri = unsigned(s->getResidue(i));
    res += 18.* _h(i, ri);
    //res += 18.* _h(i, ri);
    for(unsigned j = i + 1; j < len; ++j)
    {
      res += _J(i * len + j, ri, unsigned(s->getResidue(j)));
    }
  }
  return res;
}

HamiltonianEstimator* Hamiltonian::getEstimator() const
{
  return _myEstimator;
}

bool Hamiltonian::_thresholdPredicate(double energy, double& thetaprime) const
{
  if(energy >= _energyThreshold)
  {
    _energy_gt_thresh++;
  }
  double sigmoid = (1.0 / (1.0 + std::exp(-_sigmoidSteepness * (_energyThreshold - energy))));
  thetaprime = sigmoid * (1.0 - sigmoid);
  if(_parm->passThru)
    return true;
  double rnd = _01_distribution(_rnd_generator);
  return (rnd <= sigmoid);
}

void Hamiltonian::moreThanThresh()
{
  PLOG_DEBUG << _energy_gt_thresh << " sequences have more energy than anc";
  _energy_gt_thresh = 0;
}

void Hamiltonian::printHamiltonian(std::string suffix) // filename = outDir + h_ (or J_) + suffix;
{
  std::string h_filename = _parm->outDir + "h_" + suffix;
  std::string J_filename = _parm->outDir + "J_" + suffix;
  _h.save(h_filename, arma_ascii);
  _J.save(J_filename, arma_ascii);
}

LeafInfo::LeafInfo()
{
  this->seq = nullptr;
  this->thetaprime = 0.;
  this->energy = 0.;
  this->f1b = sp_mat();
}

LeafInfo::~LeafInfo()
{
  this->f1b.reset();
  this->seq = nullptr;
}

LeafInfo::LeafInfo(const LeafInfo& li)
{
  this->seq = li.seq;
  this->thetaprime = li.thetaprime;
  this->energy = li.energy;
  this->f1b = li.f1b;
}
