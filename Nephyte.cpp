#include "Nephyte.hpp"

Nephyte::Nephyte(Params parm)
  : _parm(parm)
{
  PLOG_DEBUG << "Initializing Nephyte...";
  // Read ancestor
  FastaFile ancfile(_parm.basetype, _parm.ancestorFile);
  // Save ancestor to use as root for trees
  _ancestor = new Sequence(ancfile.getMSA().front());
  // Get experimental MSA and pass it on to ham man. The local copy is destructed because it's semantically better.
  if(! _parm.singleStep)
  {
    FastaFile expmsafile(_parm.basetype, _parm.expmsaFile);
    std::vector<Sequence> expmsa = expmsafile.getMSA();
    _hamiltonian_man = new HamiltonianEstimator(expmsa);
    _hamiltonian_man->getF1bExp()->save(_parm.outDir + "f1b_exp.arma", arma_ascii);
    _hamiltonian_man->getF2bExp()->save(_parm.outDir + "f2b_exp.arma", arma_ascii);
    expmsa.clear();
  }
  else
    _hamiltonian_man = nullptr;
  // Load initial hamiltonian and generate object.
  mat h0;
  cube J0;
  if(_parm.h0File.empty())
    h0 = zeros(_ancestor->getLength(), _parm.basetype);
  else
    h0.load(_parm.h0File);
  if(_parm.J0File.empty())
    J0 = zeros(_ancestor->getLength() * _ancestor->getLength(), _parm.basetype, _parm.basetype);
  else
    J0.load(_parm.J0File);
  _hamiltonian = new Hamiltonian(h0, J0, _hamiltonian_man, &_parm);
}

Nephyte::~Nephyte()
{
  _trees.clear();
  delete _ancestor;
  if(_hamiltonian_man != nullptr)
    delete _hamiltonian_man;
  delete _hamiltonian;
  PLOG_VERBOSE << "Nephyte was destroyed.";
}

void Nephyte::_plantTrees(bool keepUpperBranches)
{
  for(unsigned t = 0; t < _parm.nTrees; ++t)
  {
    Node root("G0_S0", nullptr, _ancestor);
    Tree tree(root, keepUpperBranches);
    _trees.emplace_back(tree);
  }
}

void Nephyte::run()
{
  PLOG_INFO << "Starting simulation.";
  unsigned nres = _ancestor->getLength();
  unsigned max_gen = unsigned((double)nres * _parm.dissimilarity / (double)_parm.mutationsPerGeneration);
  unsigned tree_n = 0;
  unsigned generation = 0;
  std::vector<bool> isTreeDead(_parm.nTrees, false);
  std::vector<unsigned> attemptsAtTree(_parm.nTrees, 1);
  vec overprod(_parm.nTrees, fill::zeros);
  size_t nTotalStepChars = std::to_string(_parm.nSteps).size();
  #ifdef ACCURATE_MORTALITY
    uvec secondToLastLeavesCount(_parm.nTrees, fill::zeros);
    uvec lastLeavesCount(_parm.nTrees, fill::zeros);
  #endif
  auto prettyTimestep = [&](unsigned timestep)
  {
    std::string res = std::to_string(timestep);
    while(res.size() < nTotalStepChars)
      res = "0" + res;
    return res;
  };
  for(unsigned t = 1; t <= _parm.nSteps; ++t)
  {
    _trees.clear();
    attemptsAtTree.assign(attemptsAtTree.size(), 1);
    t0 = std::chrono::steady_clock::now();
    bool printingTime = (t % _parm.printStep == 0);
    _plantTrees(printingTime);
    double ancestor_energy = _hamiltonian->getSequenceEnergy(_ancestor);
    PLOG_INFO << "Ancestor energy = " << ancestor_energy;
    _hamiltonian->setEnergyThreshold(ancestor_energy);
    if(! _parm.singleStep)
      _hamiltonian_man->setCurrentStep(t);
    if(printingTime)
    {
      PLOG_DEBUG << "It's PRINTING time!";
    }
    PLOG_INFO << "Growing trees for step " << t;
    #pragma omp parallel default(shared) private(tree_n, generation)
    {
      #pragma omp for schedule(static, 1)
      for(tree_n = 0; tree_n < _parm.nTrees; tree_n++)
      {
        for(generation = 0; generation < max_gen; ++generation)
        {
          bool isFinalGeneration = (generation + 1 == max_gen);
          #ifdef ACCURATE_MORTALITY
            bool isSecondToLastGeneration = (generation + 2 == max_gen);
          #endif
          _trees.at(tree_n).spawnBranches(_parm.nBranches);
          for(unsigned m = 0; m < _parm.mutationsPerGeneration; ++m)
          {
            for(unsigned nr = 0; nr < 4; ++nr) // experimental
              _trees.at(tree_n).mutateLeaves();
            bool passDataToMan = isFinalGeneration && (m + 1 == _parm.mutationsPerGeneration);
            _trees.at(tree_n).naturalSelection(*_hamiltonian, passDataToMan);
            isTreeDead.at(tree_n) = _trees.at(tree_n).isDead();
            if(isTreeDead.at(tree_n))
            {
              if(tree_n == 0)
              {
                t1 = std::chrono::steady_clock::now();
                std::chrono::duration<unsigned> tspan_minutes = std::chrono::duration_cast<std::chrono::minutes>(t1 - t0);
                if(tspan_minutes >= std::chrono::minutes(TIMEOUT_MINS))
                {
                  PLOG_FATAL << "Timeout reached in tree generation. Exiting with status 1.";
                  exit(1);
                }
              }
              attemptsAtTree.at(tree_n)++;
              generation = 0;
              isTreeDead.at(tree_n) = false;
              Node root("G0_S0", nullptr, _ancestor);
              Tree tree(root, printingTime);
              _trees.at(tree_n).clear();
              _trees.at(tree_n) = tree;
              break;
            }
            //PLOG_INFO << "T " << tree_n << "; G " << generation << "; nleaves " << _trees.at(tree_n).getLeavesCount();
            if(_parm.maxLeaves > 0)
            {
              // Assumes that each tree roughly produces the same number of leaves
              // Given this assumption, if the expected number of leaves is too large, trim the tree down to the expected size
              double overproduction = (double) (_trees.at(tree_n).getLeavesCount() * _parm.nTrees) / (double)_parm.maxLeaves;
              //PLOG_INFO << "T " << tree_n << "; G " << generation << "; overproduction " <<overproduction;
              overprod(tree_n) = overproduction;
              if(overproduction > 1.0)
              {
                PLOG_DEBUG << "Tree " << tree_n << " is too large (overproduction of " << overproduction * 100. << "%). Pruning it at generation " << generation;
                _trees.at(tree_n).prune(1.0 - 1.0 / overproduction, isFinalGeneration, *_hamiltonian); // where the first arg is the probability of killing a leaf.
              }
            }
          }
          #ifdef ACCURATE_MORTALITY
            if(isSecondToLastGeneration)
              secondToLastLeavesCount(tree_n) = _trees.at(tree_n).getLeavesCount();
            if(isFinalGeneration)
              lastLeavesCount(tree_n) = _trees.at(tree_n).getLeavesCount();
          #endif
        }
        PLOG_INFO << "Tree " << tree_n << " complete (attempt no. " << attemptsAtTree.at(tree_n) << ")";
      }
    }
    if(_parm.maxLeaves > 0)
    {
      PLOG_INFO << "Mean leaf production: " << mean(overprod) * 100. << "% wrt max leaves";
    }
    if(printingTime)
    {
      PLOG_DEBUG << "Writing data to files.";
      auto tree_it = _trees.begin();
      auto tree_it_end = _trees.end();
      unsigned tn = 1;
      while(tree_it != tree_it_end)
      {
        if(!(tree_it->isDead()))
        {
          std::string tname = "T" + std::to_string(tn) + "_Step" + prettyTimestep(t);
          std::string nexus = tree_it->getNexus(tname);
          std::ofstream buf(_parm.outDir + tname + ".nxs");
          if(buf.good())
            buf << nexus;
          buf.close();
        }
        tree_it++;
        tn++;
        if(tn > 100)
        {
          PLOG_INFO << "Only first 100 trees will be exported to save space.";
          break;
        }
      }
      if(! _parm.singleStep)
      {
        std::vector<double> energies = _hamiltonian_man->getEnergies();
        auto en_it = energies.begin();
        auto en_it_end = energies.end();
        std::ofstream ebuf(_parm.outDir + "E_Step_" + prettyTimestep(t) + ".dat");
        for(; en_it != en_it_end && ebuf.good(); en_it++)
          ebuf << *en_it << std::endl;
        ebuf.close();
      }
    }
    if(! _parm.singleStep)
    {
      double mortality;
      #ifdef ACCURATE_MORTALITY
        mortality = 100. * double(accu(lastLeavesCount)) / (_parm.nTrees * _parm.nBranches * double(accu(secondToLastLeavesCount)));
        PLOG_INFO << "Last generation mortality = " << mortality << "%";
      #else
        mortality = 100. * (1. - double(_hamiltonian_man->getNumberOfSurvivorLeaves()) / (double(_parm.nTrees) * double(pow(_parm.nBranches, max_gen))));
        PLOG_INFO << "Mortality = " << mortality << "%";
      #endif
      const double regul_lambda = 0.001;
      double regularizator = regul_lambda * _hamiltonian->getJsquared();
      double chi2_f1b = _hamiltonian_man->getChi2F1b();
      double chi2_f2b = _hamiltonian_man->getChi2F2b(regularizator);
      PLOG_INFO << "Chi squared f1b = " << chi2_f1b;
      PLOG_INFO << "Chi squared f2b = " << chi2_f2b;
      PLOG_INFO << "Total Chi squared = " << chi2_f1b + chi2_f2b;
      PLOG_INFO << "Updating the hamiltonian.";
      _hamiltonian_man->updateHamiltonian(_hamiltonian);
      if(printingTime)
      {
        _hamiltonian->printHamiltonian("Step_" + prettyTimestep(t) + ".arma");
        _hamiltonian_man->getF1b()->save(_parm.outDir + "f1b_Step_" + prettyTimestep(t) + ".arma", arma_ascii);
        _hamiltonian_man->getF2b()->save(_parm.outDir + "f2b_Step_" + prettyTimestep(t) + ".arma", arma_ascii);
      }
      _hamiltonian_man->reset();
      PLOG_INFO << "Step " << t << " complete.";
    }
  }
}
