#include "Nephyte.hpp"
#include "Params.hpp"
#include <plog/Log.h>
#include <omp.h>

Params parseOpts(int argc, char** argv);

int main(int argc, char** argv)
{
  Params parm = parseOpts(argc, argv);
  if(parm.nThreads > 0)
    omp_set_num_threads(parm.nThreads);
  Nephyte simulation(parm);
  simulation.run();
  PLOG_INFO << "Simulation ended.";
  return 0;
}
