# Nephyte

Non-equilibrium Phylogenetic Tree Engine

## Purpose

Nephyte is a modular object oriented program aimed to the generation of phylogenetic trees
and to the reconstruction of coevolutionary potentials of proteins.

## Building

### Dependencies

In order to build the program, a compiler with support for OpenMP 3.0 and C++11 is
needed. [Armadillo](http://arma.sourceforge.net/) version >9 is needed, as well as the libraries OpenBLAS, Lapack and
HDF5.

If you do not have admin privileges on your machine you might want to consider building
Armadillo from source and then specifying the correct path to the linker. An example of
this is found in the Makefile:

```
ifeq ($(shell hostname), urano.mi.infn.it)
CXXFLAGS += -I${HOME}/armadillo/armadillo-9.400.4/include/
endif

ifeq ($(shell hostname), urano.mi.infn.it)
LDFLAGS += -L${HOME}/armadillo/armadillo-9.400.4/
endif
```

which adds header files and library objects to the compiler search paths. When employing
this solution remember to export the variable `LD_LIBRARY_PATH` both before running and
compiling. To save yourself some typing you can put the following in your `.bashrc`:

```
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HOME}/armadillo/armadillo-9.400.4/"
```

What follows is a list of compilation flags that you might want to specify, by adding them
to the `CXXFLAGS` variable in the Makefile. Use them at your own discretion.

### Compilation flags

A few variables can be set during compile time to change the behaviour of the program.
They are:

- `-DCONSISTENCY_CHECK`: enabled by default, when this variable is set, during each
  calculation of sequence energy the program checks that the potentials are of the correct
  size for the sequences it is working on. Remove this to gain a bit of extra speed, but
  be careful to pass correctly sized potentials or you are likely to get nonsensical
  results. 
- `-DCOMPATIBILITY_MODE`: this option is disabled by default. When it is enabled,
  functionalities that depend on the POSIX operating system API are disabled. This
  includes checking for existence of a directory and the ability to create a directory
  itself. If you specify this variable, then when using the `-w` option you need to make
  sure the paths you specify exist or your output will most likely get lost.
- `-DACCURATE_MORTALITY`: when this option is specified, the mortality is calculated in a
  more precise manner. Only the mortality in the last generation is calculated. If this
  option is not specified (default) behavior, a faster estimate of the mortality is made.
  This estimate is obtained by calculating the ratio of the obtained number of leaves with
  the number of leaves we would obtain if every sequence of every tree was accepted.
- `-DARMA_DONT_USE_WRAPPER`: this flag is needed by armadillo if the program is to be
  linked with directly with BLAS and LAPACK. This is especially useful to make use of the
  high performance OpenBLAS, instead of the standard BLAS.
- `-DARMA_NO_DEBUG`: when this flag is specified, armadillo does not check for out of
  bounds error when accessing the data. This squeezes out a bit of extra performance, but
  it should only be used when the program is thoroughly checked for errors.
- `-DTIMEOUT_MINS=<mins>`: specify the tree generation timeout, in minutes. Default value
  is 5. If specified in this way it must be a positive integer.

## Running

A plethora of options is available to the user. 

```
Nephyte: Non-equilibrium phylogenetic tree engine
Usage:
  ./nephyte [OPTION...]

  -s, --steps INT               Total number of steps, i.e. hamiltonian
                                updates (default: 1)
  -p, --printstep INT           Step intervals between different prints of
                                control data (default: 1)
  -b, --branches INT            Branching ratio (default: 4)
  -B, --basetype INT            Number of different residues (only 20 or 21
                                are currently supported) (default: 21)
  -c, --threads INT             Number of threads to use with openmp (0 means
                                use system default value) (default: 0)
  -d, --dissimilarity DOUBLE    Dissimilarity threshold wrt ancestor (when to
                                stop tree?) (default: 0.5)
  -S, --sigmoid-steepness DOUBLE
                                Steepness of the sigmoid acceptance predicate
                                (default: 5.0)
  -T, --trees INT               Number of trees to generate in each step
                                (default: 10)
  -f, --family FILE             Family fasta input file (default: )
  -A, --ancfile FILE            Ancestor fasta input file (default: )
  -J, --jfile FILE              Use specified armadillo tensor as initial J
                                (default: )
  -H, --hfile FILE              Use specified armadillo matrix as initial h
                                (default: )
  -w, --out-dir FILE            Save results in specified directory (default:
                                )
  -m, --mutations-per-generation INT
                                Number of punctual mutations to perform on
                                each sequence in each generation (default: 5)
  -M, --max-leaves INT          Number of maximum total leaves (0 means no
                                maximum) (default: 0)
  -P, --pass-thru               Accept all generated sequences
  -1, --single-step             Generate trees and print them, leaving the
                                hamiltonian unchanged (overrides -s and -p)
  -q, --quiet                   Quiet output. Use -qq to be even quieter.
  -v, --verbose                 Verbose output (overrides -q). Use -vv to be
                                even more verbose.
  -h, --help                    Print this help message
```

### Input files format

Apart from FASTA files, a word is due to the format of the initial potentials. The one
body potential `h` is an Armadillo matrix where interaction energy of a residue of type
`A` in position `i` is in entry `(i, A)`.

Since Armadillo, at the time of writing, only supports 3-indices tensors, the format of
the `J` potential is a bit more convoluted: the interaction energy of residue `A` in
position `i` with residue `B` in position `j` is found in entry `(i * nres + j, A, B)`
where `nres` is the number of residues.

## FAQ

##### 1. In the output Nexus files the tree structure is blank. Why?

Because the conversion algorithm to Newick format is unstable, thus it is being by-passed
in the code execution. If you need it, the function is there as a guideline but you're
going to have to put some work into it yourself to have it work properly.

##### 2. Right after the start of the program I can some error. What do I do?

Most probably, you're passing malformed input files to the program. A common error in this
case is `Error: sequence and 1 body potential use different residue numbers or
basetypes!`. Check that the ancestor input file, the MSA file and the initial potentials
(if provided) are in the right format, they exist, etc. 

##### 3. The program runs but is slow/eats a lot of memory. What do I do?

You are probably using a combination of parameters that makes the program either extremely
selective about the sequences to accept (yielding slow runtime) or not selective enough
(generating lots of sequences that build up in the RAM, especially during the Hamiltonian
update).

There is no secret ingredient here. The right combination of parameters depends on the
starting potential you are using. Trial and error will help.

##### 4. I need to modify this program or use part of it on another project. Where do I start?

The program is object oriented and most things are found in the file where you expect them
to be. If you have to understand or study the source code, start from `main.cpp` and the
`Nephyte` class. The most complicated part is the relationship between `Tree`'s natural
selection function, the `Hamiltonian` class and the `HamiltonianEstimator` class. 

It is somewhat convoluted, but it is quite efficient both from a memory and a computation
standpoint.

## License

Nephyte is Free Software, released under the GPL version 3. When I say "Free" I mean free
as in *free speech* not as in *free beer*. To learn more about this you can check out the
[Free Software Foundation](https://fsf.org).
