#pragma once

#include <plog/Log.h>
#include <armadillo>
#include <cmath>
#include <chrono>
#include <random>
#include <vector>
#include <algorithm>
#include <omp.h>
#include "Sequence.hpp"
#include "Params.hpp"

using namespace arma;

class HamiltonianEstimator;

class Hamiltonian
{
  // The estimator will directly manipulate _h and _J
  friend class HamiltonianEstimator;
  public:
    Hamiltonian(mat h, cube J, HamiltonianEstimator* estimator, Params* parm);
    bool validate(Sequence* s, bool isFinalGeneration) const;
    double getSequenceEnergy(Sequence* s) const;
    void setSigmoidSteepness(double x);
    void setEnergyThreshold(double x);
    double getJsquared() const;
    void moreThanThresh();
    void printHamiltonian(std::string suffix); // filename = h_ (or J_) + suffix;
    HamiltonianEstimator* getEstimator() const;
  private:
    bool _thresholdPredicate(double energy, double& thetaprime) const;
    Params* _parm;
    mat _h;
    cube _J;
    mutable unsigned _energy_gt_thresh;
    double _sigmoidSteepness;
    double _energyThreshold;
    HamiltonianEstimator* _myEstimator;
    mutable std::default_random_engine _rnd_generator;
    mutable std::uniform_real_distribution<double> _01_distribution;
};

struct LeafInfo
{
  LeafInfo();
  ~LeafInfo();
  LeafInfo(const LeafInfo& li);
  Sequence* seq;
  double thetaprime;
  double energy;
  sp_mat f1b;
};

class HamiltonianEstimator
{
  // The hamiltonian will pass useful results (thetaprime, ecc) to the estimator to avoid repeating the same calculations twice
  friend class Hamiltonian;
  public:
    HamiltonianEstimator(std::vector<Sequence> expmsa);
    void setCurrentStep(unsigned step);
    void updateHamiltonian(Hamiltonian* H);
    unsigned getNumberOfSurvivorLeaves();
    std::vector<double> getEnergies() const;
    arma::mat* getF1b();
    arma::cube* getF2b();
    arma::mat* getF1bExp();
    arma::cube* getF2bExp();
    double getChi2F1b();
    double getChi2F2b(const double& regul = 0.);
    bool deleteLeafInfo(Sequence* s);
    void reset();
  private:
    mat _f1b;
    cube _f2b;
    mat _f1b_exp;
    cube _f2b_exp;
    unsigned _current_step;
    bool _no_survivors;
    std::vector<Sequence> _exp_msa;
    std::vector<LeafInfo> _leaves_info;   
    bool _hasFrequencies;
    void _calculateFrequencies();
    void _init();
};
