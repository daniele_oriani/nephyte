CXXFLAGS = -g -Wall -std=c++11 -O3 -DCONSISTENCY_CHECK -DARMA_DONT_USE_WRAPPER -fopenmp -Iinclude

ifeq ($(shell hostname), urano.mi.infn.it)
CXXFLAGS += -I${HOME}/armadillo/armadillo-9.400.4/include/
endif

LDFLAGS = -larmadillo -lopenblas -llapack -lhdf5

ifeq ($(shell hostname), urano.mi.infn.it)
LDFLAGS += -L${HOME}/armadillo/armadillo-9.400.4/
endif

OBJECTS = Sequence.o Node.o Tree.o Hamiltonian.o HamiltonianEstimator.o FastaFile.o Nephyte.o CLI.o main.o

all: nephyte

nephyte: $(OBJECTS)
	$(LINK.cc) $^ -o $@

%.o: %.cpp %.hpp
	$(COMPILE.cc) $< -o $@

.PHONY: clean cleanres

clean:
	$(RM) *.o

cleanres:
	$(RM) *.nxs *.dat *.arma
