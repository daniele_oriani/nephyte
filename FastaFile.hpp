#pragma once

#include <plog/Log.h>
#include <fstream>
#include <vector>
#include "Sequence.hpp"

class FastaFile
{
  public:
    FastaFile(const unsigned basetype, std::string filename);
    std::vector<Sequence> getMSA();
  private:
    unsigned _basetype;
    std::string _filename;
    std::vector<Sequence> _sequences;
    std::ifstream _buf;
};
