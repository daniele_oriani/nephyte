#include <iostream>
#include <algorithm>
#include <fstream>
#include <vector>

// Usage: thresholdplot <candidatecontactsfile> <referencecontactsfile>
// The files should be in format
// <i>  <j>  <score>
// And already sorted in descending order of score
// Output is in format
// <threshold>  <truepositives>  <falsepositives>

const size_t col_width = 12;
auto rjust = [](std::string s){ while(s.length() < col_width) s += " "; return s;};

struct DataPoint
{
  double threshold;
  unsigned truepos;
  unsigned falsepos;
  void print() const
  {
    std::cout << rjust(std::to_string(threshold)) << " " << rjust(std::to_string(truepos)) << " " << rjust(std::to_string(falsepos)) << std::endl;
  }
};

struct Contact
{
  unsigned i;
  unsigned j;
  double score;
  Contact(const unsigned& ii, const unsigned& jj, const double& sscore)
    :i(ii), j(jj), score(sscore)
  {
  }
  void print() const
  {
    std::cout << i << "  " << j << "  " << score << std::endl;
  }
};

std::vector<Contact> readContactsFile(const std::string& filename)
{
  std::ifstream buf(filename);
  std::vector<Contact> res;
  unsigned i, j;
  double score;
  while(buf.good())
  {
    buf >> i >> j >> score;
    if(i >= j)
    {
      // Ensure i < j always. Input files should not have any doubles (ie. if it has contact x-y it should not have contact y-x)
      unsigned tmp = i;
      i = j;
      j = tmp;
    }
    res.push_back(Contact(i, j, score));
  }
  buf.close();
  return res;
}

int main(int argc, char** argv)
{
  std::vector<Contact> candidate_cc;
  std::vector<Contact> reference_cc;
  std::vector<DataPoint> res;
  unsigned trueposcount = 0;
  unsigned falseposcount = 0;
  candidate_cc = readContactsFile(argv[1]);
  reference_cc = readContactsFile(argv[2]);
  auto cand_it = candidate_cc.begin();
  auto cand_it_end = candidate_cc.end();
  auto notfound = reference_cc.end();
  for(; cand_it != cand_it_end; ++cand_it)
  {
    auto ref_it = std::find_if(reference_cc.begin(), reference_cc.end(), [&](const Contact& c1){ return ((c1.i == cand_it->i) && (c1.j == cand_it->j)); }); 
    if(ref_it == notfound)
    {
      falseposcount++;
    }
    else
    {
      trueposcount++;
    }
    DataPoint p;
    p.threshold = cand_it->score;
    p.truepos = trueposcount;
    p.falsepos = falseposcount;
    res.push_back(p);
  }
  auto res_it = res.begin();
  auto res_it_end = res.end();
  for(; res_it != res_it_end; ++res_it)
  {
    res_it->print();
  }
}
