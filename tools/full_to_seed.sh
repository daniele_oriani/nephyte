#!/bin/bash

# USAGE: ./full_to_seed.sh <full> <seed>

FULL="$1"
SEED="$2"

names=$(awk '/^>/{print(substr($0,2))}' $SEED)

for name in ${names}
do
  awk -v nm=$name 'BEGIN{record=0;} /^>/{if(record == 1) record = 0;} {if(record == 1){print($0);}} $0 ~ nm {record=1; print($0);}' $FULL
done
