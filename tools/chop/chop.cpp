#include <fstream>
#include <iostream>
#include <vector>
#include <string>

//Remove residues where the ancestor (the first sequence of the input MSA) has gaps
//Usage: ./chop <fastafile>

const unsigned basetype = 21;
typedef std::vector<std::string> msa_t;

msa_t readFile(std::string filename)
{
  std::ifstream buf(filename); 
  msa_t res;
  std::string s = "";
  std::string seq = "";
  std::getline(buf, s);
  unsigned seqno = 1;
  while(buf.good())
  {
    if(s.front() != '>')
    {
      seq.append(s);
    }
    else
    {
      if(seqno > 1)
      {
        res.push_back(seq);
        seq = "";
      }
      seqno++;
    }
    std::getline(buf, s);
  }
  res.push_back(seq);
  buf.close();
  return res;
}

int main(int argc, char** argv)
{
  msa_t msa = readFile(argv[1]);
  std::string anc_mask = msa.front();
  for(unsigned seq = 1; seq < msa.size(); ++seq)
  {
    auto it = msa.at(seq).begin();
    auto it_end = msa.at(seq).end();
    unsigned r = 0;
    unsigned dels = 0;
    while(it != it_end)
    {
      if(anc_mask.at(r) == '-')
      {
        msa.at(seq).erase(it);
        it = msa.at(seq).begin() + r - dels;
        it_end = msa.at(seq).end();
        dels++;
      }
      else
      {
        it++;
      }
      r++;
    }
  }
  auto it_s = msa.begin();
  auto it_s_end = msa.end();
  for(; it_s != it_s_end; ++it_s)
  {
    std::cout << *it_s << std::endl << std::endl;
  }
}
