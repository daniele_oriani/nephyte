#!/usr/bin/awk -f

BEGIN{
  atom_serial_shift = 233
  residue_shift = 15
}

/^ATOM /{
  piece1 = substr($0, 1, 14)
  piece2 = substr($0, 15) 
  serial = $2
  residue = $6
  newserial = serial - atom_serial_shift
  seriallendiff = - length(newserial) + length(serial)
  for(i=1; i <= seriallendiff; i++){
    newserial = " " newserial  
  }
  newresidue = residue - residue_shift
  reslendiff = - length(newresidue) + length(residue)
  for(i=1; i<=reslendiff; i++){
    newresidue = " " newresidue
  }
  sub(serial, newserial, piece1)
  sub(residue, newresidue, piece2)
  #$6 = residue
  res = piece1 piece2
  print(res)
}
