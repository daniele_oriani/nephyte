#!/bin/bash

while true
do
  touch ../lastwatch
  echo Going to sleep for a minute...
  sleep 60
  for D in $(find ../ -cnewer "../lastwatch" -type d -name "2ABP*")
  do
    echo "Processing $procdir"
    procdir=${D/*\//}
    mkdir -p $procdir
    jfile="$(find $D -name "J_Step_*" | tail -n 1)"
    hfile="$(find $D -name "h_Step_*" | tail -n 1)"
    if [ -z "$hfile" ] 
    then
      echo "Skipping: simulation incomplete"
      continue
    else
      ln -s -t "$procdir" "../$hfile" 
    fi
    if [ -z "$jfile" ] 
    then
      echo "Skipping: simulation incomplete"
      continue
    else
      ln -s -t "$procdir" "../$jfile" 
    fi 
    echo "Calculating DI"
    ./DI/di $procdir/h_Step_* $procdir/J_Step_* > $procdir/neph_contacts.dat
    echo "Calculating contact thresh plot data"
    ./contactCountPlot.sh $procdir/neph_contacts.dat contacts/pdbcontacts > $procdir/contact_thresh_plot.dat
    echo "Done processing $procdir"
  done
done
