#!/bin/bash

# USAGE: ./uncollider.sh <filetouncollide>

COLLISIONS=$(awk '/^>/{print(substr($0, 2, 10))}' "$1" | sort | uniq -d)
COUNTER=1

cp $1 $1.bak
rm -f collisions.dict

# Loop over all colliding names (one cycle each)
for coll in ${COLLISIONS}
do
  # Save all the names that produce that collision
  oldname=$(awk -v COLL="${coll}" '$0 ~ COLL {print $0}' $1)
  # Cycle over them
  for n in ${oldname}
  do
    # Save the correspondence oldname === newname in the file collisions.dict
    echo "${n} === COLL${COUNTER}" >> collisions.dict
    # Make a temp file where the new FASTA with this collision resolved saved
    tmp=$(mktemp)
    # Resolve collision and save to temp file
    awk -v NAME="${n}" -v C=${COUNTER} '$0 !~ NAME {print $0}; $0 ~ NAME {$1 = ">COLL" C; print $0}' $1 > ${tmp}
    # Replace input file with temp file
    rm $1
    mv ${tmp} $1
    # Increase counter
    COUNTER=$((COUNTER + 1))
  done

done
