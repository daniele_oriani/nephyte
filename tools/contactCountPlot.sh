#!/bin/bash

# USAGE: contactCountPlot.sh <frobScorefile> <pdbcontactsfile>

PDBCONTACTS=$2
FROBFILE=$1
NFROB=$(cat "$FROBFILE" | wc -l)

echo Frobthresh, true positive, false positive, total contacts

for lineno in $(seq 1 $NFROB)
do
  total=$lineno
  frobthresh=$(sed -n -e "$lineno p" $FROBFILE | awk '{print $3}')
  truepos=$(while read -r line
  do
    i=$(echo $line | awk '{print $1}')
    j=$(echo $line | awk '{print $2}')
    if [[ $i -lt $j ]]
    then
      first=$i
      second=$j
    else
      first=$j
      second=$i
    fi
    regex="^ +$first +$second +"
    egrep "$regex" "$2"
  done < <(sed -n -e "1, $lineno p" $FROBFILE) | wc -l)
  falsepos=$(( $total - $truepos ))
  echo $frobthresh $truepos $falsepos $total
done
