#include <armadillo>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace arma;

// usage: J_to_frobenius <tensor.arma>

const size_t col_width = 12;
auto rjust = [](std::string s){ while(s.length() < col_width) s += " "; return s;};

struct FrobScore
{
    unsigned i;
    unsigned j;
    double score;
    FrobScore(unsigned pos_i, unsigned pos_j, double frobeniusNorm)
      : i(pos_i), j(pos_j), score(frobeniusNorm)
    {
    }
    bool operator<(const FrobScore& rhs)
    {
      return (this->score < rhs.score);
    }
    void print() const
    {
      std::cout << rjust(std::to_string(i)) << " " << rjust(std::to_string(j)) << " " << rjust(std::to_string(score)) << std::endl;
    }
};

int main(int argc, char** argv)
{
  cube J;
  std::vector<FrobScore> frob;
  unsigned nres;
  unsigned basetype;
  double FNij;
  J.load(argv[1]);
  nres = ::sqrt(J.n_rows);
  basetype = J.n_slices;
  for(unsigned i = 0; i < nres; ++i)
  {
    for(unsigned j = i + 1; j < nres; ++j)
    {
      FNij = 0.;
      for(unsigned A = 0; A < basetype; ++A)
      {
        for(unsigned B = 0; B < basetype; ++B)
        {
          FNij += J(i * nres + j, A, B) * J(i * nres + j, A, B);
        }
      }
      frob.push_back(FrobScore(i + 1, j + 1, ::sqrt(FNij)));
    }
  }
  std::sort(frob.rbegin(), frob.rend());
  auto it = frob.begin();
  auto it_end = frob.cend();
  auto max_frob = frob.front().score;
  std::cout << rjust("i") << " " << rjust("j") << " " << rjust("score") << std::endl << std::endl;
  for(; it != it_end; ++it)
  {
    it->score /= max_frob;
    if(it->i < it->j)
      it->print();
  }
  return 0;
}
