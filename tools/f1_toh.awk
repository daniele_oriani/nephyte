#!/usr/bin/awk -f

BEGIN{
  avg = 0.047619;
  min = 6.64187035069075e-05;
}

{
  if(NR > 2)
  {
    for(i = 1; i <= NF; i++)
    {
      if(i == 1)
      {
        printf("  %e", 0.);
        continue;
      }
      if($i == 0.)
      {
        printf("  %e", -1.0 * log(min/avg));  
      }
      else
      {
        printf("  %e", -1.0* log($i/avg));
      }
    }  
    printf("\n");
  }  
}
