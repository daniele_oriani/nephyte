#include <armadillo>
#include <iostream>
#include <string>

using namespace arma;

//USAGE: gaptozero <hfile> <jfile>

int main(int argc, char** argv)
{
  mat H;
  cube J;
  unsigned nres;
  unsigned basetype;
  H.load(argv[1]);
  nres = H.n_rows;
  basetype = H.n_cols;
  std::cout << "nres = " << nres << "; basetype = " << basetype << std::endl;
  J.load(argv[2]);
  for(unsigned i = 0; i < nres; ++i)
  {
    for(unsigned A = 0; A < basetype; ++A)
    {
      if(A == 0)
      {
        H(i, A) = 0.;
      }
      for(unsigned j = i + 1; j < nres; ++j)
      {
        for(unsigned B = 0; B < basetype; ++B)
        {
          if(A == 0 || B == 0)
          {
            J(i * nres + j, A, B) = 0.;
            J(j * nres + i, B, A) = 0.;
          }
        }
      }
    }
  }
  H.save(std::string(argv[1]) + ".gapz", arma_ascii);
  J.save(std::string(argv[2]) + ".gapz", arma_ascii);
  return 0;
}
