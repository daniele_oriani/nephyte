#!/bin/bash

hamming=(0.3 0.6 0.8)

for dist in ${hamming[@]}
do
  file="neph_d${dist}.dat"
  echo -e "branches\tmutations\tsteps" > $file
  for dir in *_d${dist}_*
  do
    m=$(echo $dir | sed -n -r 's/^1BPI.*_m([[:digit:]]+).*$/\1/p')
    b=$(echo $dir | sed -n -r 's/^1BPI.*_b([[:digit:]]+).*$/\1/p')
    steps=$(
    tac $dir/nephyte.log | awk 'BEGIN{code=1;} {if(match($0, "Step ([[:digit:]]+) complete.", groups)){print(groups[1]); code=0; exit(code);}} END{exit(code);}' || echo 0
    )
    echo -e "$b\t$m\t$steps" >> $file
  done
done
