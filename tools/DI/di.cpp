#include <armadillo>
#include <string>
#include <vector>
#include <iostream>

// USAGE: ./di <hfile> <jfile>
//

const size_t col_width = 12;
auto rjust = [](std::string s){ while(s.length() < col_width) s += " "; return s;};

struct DIscore
{
    unsigned i;
    unsigned j;
    double discore;
    DIscore(unsigned pos_i, unsigned pos_j, double di)
      : i(pos_i), j(pos_j), discore(di)
    {
    }
    bool operator<(const DIscore& rhs)
    {
      return (this->discore < rhs.discore);
    }
    void print() const
    {
      std::cout << rjust(std::to_string(i)) << " " << rjust(std::to_string(j)) << " " << rjust(std::to_string(discore)) << std::endl;
    }
};

int main(int argc, char** argv)
{
  arma::mat h;
  arma::cube J;
  h.load(argv[1]);
  J.load(argv[2]);
  unsigned nres = h.n_rows;
  unsigned nbase = h.n_cols;
  arma::mat Z_ij(nres, nres, arma::fill::zeros);
  arma::cube P_ij(nres * nres, nbase - 1, nbase - 1, arma::fill::zeros);
  arma::mat f_i(nres * nres, nbase - 1, arma::fill::zeros);
  std::vector<DIscore> ranking;
  double tmp = 0;
  for(unsigned int i = 0; i < nres; ++i)
  {
    for(unsigned int j = 0; j < nres; ++j)
    {
      for(unsigned int A = 1; A < nbase; ++A)
      {
        for(unsigned int B = 1; B < nbase; ++B)
        {
          tmp = 0;
          tmp += h(i, A);
          tmp += h(j, B);
          tmp += J(i * nres + j, A, B);
          Z_ij(i, j) += exp(tmp);
        }
      }
    }
  }
  for(unsigned int i = 0; i < nres; ++i)
  {
    for(unsigned int j = 0; j < nres; ++j)
    {
      for(unsigned int A = 1; A < nbase; ++A)
      {
        for(unsigned int B = 1; B < nbase; ++B)
        {
          tmp = 0;
          tmp += h(i, A);
          tmp += h(j, B);
          tmp += J(i * nres + j, A, B);
          P_ij(i * nres + j, A - 1, B - 1) = exp(tmp) / Z_ij(i, j);
        }
      }
    }
  }
  for(unsigned int i = 0; i < nres; ++i)
  {
    for(unsigned int j = 0; j < nres; ++j)
    {
      for(unsigned int A = 0; A < nbase - 1; ++A)
      {
        for(unsigned int B = 0; B < nbase - 1; ++B)
        {
          f_i(i * nres + j, A) += P_ij(i * nres + j, A, B);
        }
      }
    }
  }
  for(unsigned int i = 0; i < nres; ++i)
  {
    for(unsigned int j = i; j < nres; ++j)
    {
      double diij = 0.;
      for(unsigned int A = 0; A < nbase - 1; ++A)
      {
        for(unsigned int B = 0; B < nbase - 1; ++B)
          diij += P_ij(i * nres + j, A, B) * log(P_ij(i * nres + j, A, B) / (f_i(i * nres + j, A) * f_i(j * nres + i, B)));
      }
      ranking.push_back(DIscore(i, j, diij));
    }
  }
  std::sort(ranking.rbegin(), ranking.rend());
  auto it = ranking.begin();
  auto it_end = ranking.cend();
  auto max_di = ranking.front().discore;
  std::cout << rjust("i") << " " << rjust("j") << " " << rjust("score") << std::endl << std::endl;
  for(; it != it_end; ++it)
  {
    it->discore /= max_di;
    if(it->i < it->j)
      it->print();
  }
  return 0;

}
