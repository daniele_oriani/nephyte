#!/bin/bash

# USAGE: contactComp.sh <frobScoreFile> <pdbcontacts>
# where frobScoreFile is obtained from J_to_frobenius (with trimmed header)
# and pdbcontacts is obtained from pdb2br_all (without -map)

while read -r line
do
  i=$(echo $line | awk '{print $1}')
  j=$(echo $line | awk '{print $2}')
  if [[ $i -lt $j ]]
  then
    first=$i
    second=$j
  else
    first=$j
    second=$i
  fi
  regex="^ +$first +$second +"
  egrep "$regex" "$2"
done < "$1"
