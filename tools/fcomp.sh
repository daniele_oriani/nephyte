#!/bin/bash

# USAGE: copy in the directory with results and run.

echo "Flattening arma files..."

for f in *.arma
do
  [[ ! -e "$f.flat" ]] &&  ../../../../tools/flatten.awk "$f" > "$f.flat"
done

echo "Generating f1b comp files..."

for f in f1b*.arma.flat
do
  [[ "$f" == "f1b_exp.arma.flat" ]] && continue
  a=${f/f1b_Step_/}
  paste "f1b_exp.arma.flat" "$f" > f1b_comp_${a/.arma.flat/}.dat
done

echo "Generating f2b comp files..."

for f in f2b*.arma.flat
do
  [[ "$f" == "f2b_exp.arma.flat" ]] && continue
  a=${f/f2b_Step_/}
  paste "f2b_exp.arma.flat" "$f" > f2b_comp_${a/.arma.flat/}.dat
done
